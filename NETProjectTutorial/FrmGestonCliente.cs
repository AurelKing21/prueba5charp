﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestonCliente : Form
    {
        private DataSet dsProductos;
        private BindingSource bsProductos;

        public DataSet DsProductos { set { dsProductos = value; } }
        public FrmGestonCliente()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

        private void FrmGestonCliente_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = dsProductos;
            bsProductos.DataMember = dsProductos.Tables["Clientes"].TableName;
            dataGridView1.DataSource = bsProductos;
            dataGridView1.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblClientes = dsProductos.Tables["Clientes"];
            fc.DsClientes = dsProductos;
            fc.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
        }

        private void bntDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dsProductos.Tables["Producto"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsProductos.Filter = string.Format("Cedula like '*{0}*' or Nombre like '*{0}*' or Apellido like '*{0}*' ", textBox1.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
