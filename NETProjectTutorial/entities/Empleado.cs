﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        private int id;
        private string inss;
        private string cedula;
        private string nombre;
        private string apellido;
        private string direccion;
        private string tconvencional;
        private string tcelular;
        private SEXO sexo;
        private double salario;

        public Empleado(int id, string inss, string cedula, string nombre, string apellido, string direccion, string tconvencional, string tcelular, SEXO sexo, double salario)
        {
            this.id = id;
            this.inss = inss;
            this.cedula = cedula;
            this.nombre = nombre;
            this.apellido = apellido;
            this.direccion = direccion;
            this.tconvencional = tconvencional;
            this.tcelular = tcelular;
            this.sexo = sexo;
            this.salario = salario;
        }
        public Empleado()
        {

        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Tconvencional
        {
            get
            {
                return tconvencional;
            }

            set
            {
                tconvencional = value;
            }
        }

        public string Tcelular
        {
            get
            {
                return tcelular;
            }

            set
            {
                tcelular = value;
            }
        }

        public SEXO Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public double Salario
        {
            get
            {
                return salario;
            }

            set
            {
                salario = value;
            }
        
    
        }
    }
}
