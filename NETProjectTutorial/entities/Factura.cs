﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;
        private string cod_factura;
        private DateTime fecha;
        private Empleado empleado;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;
        private Cliente cliente;

        public Factura(int id, string cod_factura, DateTime fecha, Empleado empleado, string observaciones, double subtotal, double iva, double total, Cliente cliente)
        {
            this.Id1 = id;
            this.Cod_factura1 = cod_factura;
            this.Fecha1 = fecha;
            this.Empleado1 = empleado;
            this.Observaciones1 = observaciones;
            this.Subtotal1 = subtotal;
            this.Iva1 = iva;
            this.Total1 = total;
            this.Cliente = cliente;
        }

        public int Id
        {
            get
            {
                return Id1;
            }

            set
            {
                Id1 = value;
            }
        }

        public string Cod_factura
        {
            get
            {
                return Cod_factura1;
            }

            set
            {
                Cod_factura1 = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return Fecha1;
            }

            set
            {
                Fecha1 = value;
            }
        }

        internal Empleado Empleado
        {
            get
            {
                return Empleado1;
            }

            set
            {
                Empleado1 = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return Observaciones1;
            }

            set
            {
                Observaciones1 = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return Subtotal1;
            }

            set
            {
                Subtotal1 = value;
            }
        }

        public double Iva
        {
            get
            {
                return Iva1;
            }

            set
            {
                Iva1 = value;
            }
        }

        public double Total
        {
            get
            {
                return Total1;
            }

            set
            {
                Total1 = value;
            }
        }

        public int Id1
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cod_factura1
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha1
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        internal Empleado Empleado1
        {
            get
            {
                return empleado;
            }

            set
            {
                empleado = value;
            }
        }

        public string Observaciones1
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public double Subtotal1
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva1
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total1
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        internal Cliente Cliente
        {
            get
            {
                return cliente;
            }

            set
            {
                cliente = value;
            }
        }
    }
}
