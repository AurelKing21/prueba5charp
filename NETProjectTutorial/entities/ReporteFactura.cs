﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        private string cod_Factura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;

        // detalle factura
        private int cantidad;
        private double precio;


        //Producto
        private string sku;
        private string nombre_producto;

        //empleado
        private string nombre_empleado;
        private string apellido_empleado;

        public ReporteFactura(string cod_Factura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, string sku, string nombre_producto, string nombre_empleado, string apellido_empleado)
        {
            this.cod_Factura = cod_Factura;
            this.fecha = fecha;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.cantidad = cantidad;
            this.precio = precio;
            this.sku = sku;
            this.nombre_producto = nombre_producto;
            this.nombre_empleado = nombre_empleado;
            this.apellido_empleado = apellido_empleado;
        }

        public string Cod_Factura
        {
            get
            {
                return cod_Factura;
            }

            set
            {
                cod_Factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre_producto
        {
            get
            {
                return nombre_producto;
            }

            set
            {
                nombre_producto = value;
            }
        }

        public string Nombre_empleado
        {
            get
            {
                return nombre_empleado;
            }

            set
            {
                nombre_empleado = value;
            }
        }

        public string Apellido_empleado
        {
            get
            {
                return apellido_empleado;
            }

            set
            {
                apellido_empleado = value;
            }
        }
    }
}
