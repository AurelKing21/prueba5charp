﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsProductos;
        private BindingSource bsProductos;
        private DataRow drClientes;
        public FrmCliente()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }
        public DataTable TblClientes{ set { tblClientes = value; } }
        public DataSet DsClientes { set { dsProductos = value; } }
        public DataRow DrProducto
        {
            set
            {
                drClientes = value;
                txtCed.Text = drClientes["Cedula"].ToString();
                txtName.Text = drClientes["Nombre"].ToString();
                txtApellid.Text = drClientes["Apellidos"].ToString();
                txtTel.Text = drClientes["Telefono"].ToString();
                txtCorreo.Text = drClientes["Correo"].ToString();
                txtDir.Text = drClientes["Direccion"].ToString();

            }

        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = dsProductos;
            bsProductos.DataMember = dsProductos.Tables["Clientes"].TableName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cedu, nombre, apellido, correo, direcc;
            int tel;

            cedu = txtCed.Text;
            nombre = txtName.Text;
            apellido = txtApellid.Text;
            correo = txtCorreo.Text;
            direcc = txtDir.Text;
            tel = Int32.Parse(txtTel.Text);

            if (drClientes != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drClientes);
                drNew["Id"] = drClientes["Id"];
                drNew["Cedula"] =cedu;
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellido;
                drNew["Telefono"] = tel;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direcc;


                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);

            }
            else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1,cedu,nombre,apellido,tel,correo,direcc);
            }

            Dispose();
        }
        }
    }

