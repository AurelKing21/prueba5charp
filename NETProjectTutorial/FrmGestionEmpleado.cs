﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleado : Form
    {
        private DataSet  dsEmpleado;
        private BindingSource  bsEmpleado;

        public FrmGestionEmpleado()
        {
            InitializeComponent();
            bsEmpleado = new BindingSource();
        }
        public DataSet DsEmpleados
        {
            set
            {
                dsEmpleado = value;
            }
        }
        private void FrmGestionEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleado.DataSource = dsEmpleado;
            bsEmpleado.DataMember = dsEmpleado.Tables["Empleados"].TableName;
            dgvEmpleado.DataSource = bsEmpleado;
            dgvEmpleado.AutoGenerateColumns = true;
        }
    }
}
