﻿namespace NETProjectTutorial
{
    partial class FrmReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvReporte = new System.Windows.Forms.DataGridView();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnVerReport = new System.Windows.Forms.Button();
            this.txtFinderReport = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvReporte
            // 
            this.dgvReporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReporte.Location = new System.Drawing.Point(12, 105);
            this.dgvReporte.Name = "dgvReporte";
            this.dgvReporte.Size = new System.Drawing.Size(846, 370);
            this.dgvReporte.TabIndex = 0;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(303, 492);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(75, 23);
            this.btnNuevo.TabIndex = 1;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnVerReport
            // 
            this.btnVerReport.Location = new System.Drawing.Point(432, 492);
            this.btnVerReport.Name = "btnVerReport";
            this.btnVerReport.Size = new System.Drawing.Size(75, 23);
            this.btnVerReport.TabIndex = 2;
            this.btnVerReport.Text = "Ver";
            this.btnVerReport.UseVisualStyleBackColor = true;
            this.btnVerReport.Click += new System.EventHandler(this.btnVerReport_Click);
            // 
            // txtFinderReport
            // 
            this.txtFinderReport.Location = new System.Drawing.Point(12, 46);
            this.txtFinderReport.Multiline = true;
            this.txtFinderReport.Name = "txtFinderReport";
            this.txtFinderReport.Size = new System.Drawing.Size(846, 28);
            this.txtFinderReport.TabIndex = 3;
            this.txtFinderReport.TextChanged += new System.EventHandler(this.txtFinderReport_TextChanged);
            // 
            // FrmReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 527);
            this.Controls.Add(this.txtFinderReport);
            this.Controls.Add(this.btnVerReport);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.dgvReporte);
            this.Name = "FrmReportes";
            this.Text = "FrmReportes";
            this.Load += new System.EventHandler(this.FrmReportes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvReporte;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnVerReport;
        private System.Windows.Forms.TextBox txtFinderReport;
    }
}