﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFactura : Form
    {
        private DataSet dsSistema;
        public FrmReporteFactura()
        {
            InitializeComponent();
        }

        public DataSet Dssitema
        {
            set
            {
                dsSistema = value;
            }
        }
    
          

        private void FrmReporteFactura_Load(object sender, EventArgs e)
        {
            dsSistema.Tables["ReporteFactura"].Rows.Clear();

            DataTable dtFactura = dsSistema.Tables["Factura"];
            int countFactura = dtFactura.Rows.Count;

            DataRow drFactura = dtFactura.Rows[countFactura - 1];
            DataRow drEmpleado = dsSistema.Tables["Empleados"].Rows.Find(drFactura["Empleado"]);
            DataRow drCliente = dsSistema.Tables["Clientes"].Rows.Find(drFactura["Cliente"]);

            DataTable dtDetallefactura = dsSistema.Tables["DetalleFactura"];

            DataRow[] drDetalleFactura =
                dtDetallefactura.Select(string.Format("Factura = {0}", drFactura["Id"]));

            foreach(DataRow  dr in drDetalleFactura)
            {
                DataRow drReporteFactura = dsSistema.Tables["ReporteFactura"].NewRow();
                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Producto"]);
                drReporteFactura["Cod_Factura"] = drFactura["CodFactura"];
                  drReporteFactura["Fecha"] = drFactura["Fecha"];
                drReporteFactura["SubTotal"] = drFactura["SubTotal"];
                drReporteFactura["IVA"] = drFactura["Iva"];
                drReporteFactura["Total"] = drFactura["Total"];
                drReporteFactura["Nombre_Empleado"] = drEmpleado["Nombres"];
                drReporteFactura["Apellido_Empleado"] = drEmpleado["Apellidos"];
                drReporteFactura["Nombre_Cliente"] = drCliente["Nombre"];
                drReporteFactura["SKU"] = drProducto["SKU"];
                drReporteFactura["Nombre_Producto"] = drProducto["Nombre"];
                drReporteFactura["Cantidad"]= dr["Cantidad"];
                drReporteFactura["Precio"] = dr["CodFactura"];

                dsSistema.Tables["ReporteFactura"].Rows.Add(drReporteFactura);

            }

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NetProjectTutorial.ReporteFactura.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsReporteFactura", dsSistema.Tables["ReporteFactura"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);

            this.reportViewer1.RefreshReport();
        }
    }
}
