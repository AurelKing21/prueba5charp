﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListClientes = new List<Cliente>();
        private implements.DaoImplementsCliente daocliente;
        public ClienteModel()
        {
            daocliente = new implements.DaoImplementsCliente();
        }


        public  List<Cliente> GetListCliente()
        {
            return ListClientes;
        }
        public  void populate()
        {
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_DATA);
                foreach(Cliente c in ListClientes)
            {
                daocliente.save(c);
            }
        }
    }
}
