﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using Newtonsoft.Json;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados = new List<Empleado>();
         

        public static List<Empleado> GetLstEmpleado()
        {
            return Lstempleados;
        }

        public static void Populate()
        {
            // Empleado[] empleado =
            //  {
            //     new Empleado(1,"1658574","001-110288-5588D","Pepito","Perez","del arbolito 2C. abajo","22558877","88774455",SEXO.Male,15654.45),
            //     new Empleado(2,"1654567","002-120399-5599D","Ana","Conda","de  la racachaca 3. al sur","22557845","88785497",SEXO.Female,157567.45),
            //     new Empleado(3,"1657896","003-140799-5658D","Armando","Guerra","de las delicias del volga 1/2 al sur","22558877","88774589",SEXO.Male,154537.45)
            //  };
            Lstempleados = JsonConvert.DeserializeObject<List<Empleado>>
                (System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_DATA));
        }
       
    }
}
