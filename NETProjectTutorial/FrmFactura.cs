﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{


    public partial class FrmFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private Double total;
        private double Iva = 0;
        private double subtotal;
        private bool _canUpdate = true;
        private bool _needUpdate  = true;
        private string cod_Factura;
        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }
        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        public object DataTime { get; private set; }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleado.DataSource = dsSistema.Tables["Empleados"];
            cmbEmpleado.DisplayMember = "NA";
            cmbEmpleado.ValueMember = "Id";

            cmbProducto.DataSource = dsSistema.Tables["Producto"];
            cmbProducto.DisplayMember = "SKUN";
            cmbProducto.ValueMember = "Id";

            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;

            cod_Factura = "FA" + dsSistema.Tables["Factura"].Rows.Count + 1;
            txtCode.Text = cod_Factura;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void cmbProducto_TabIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)cmbProducto.SelectedItem).Row;
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);

            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "ERROR, Producto ya agregado , Verifique por favor!", "Mesaje Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            CalcularFacturaTotal();
        }

        private void dgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrProductoFactura = dgvProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura.DataBoundItem).Row;

            DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);

            if (Int32.Parse(drProductoFactura["Cantidad"].ToString()) > Int32.Parse(drProducto["Cantidad"].ToString()))
            {

                MessageBox.Show(this, "Error,La cantidad de productos a vender  no pueden ser mayor que la del almacen", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = Int32.Parse(drProducto["Cantidad"].ToString());
            }

            CalcularFacturaTotal();

        }
        private void CalcularFacturaTotal()
        {
            subtotal = 0;

            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += Int32.Parse(dr["Cantidad"].ToString()) * double.Parse(dr["Precio"].ToString());
            }

            Iva = subtotal * 0.15;
            total = subtotal + Iva;

            txtSubtotal.Text = subtotal.ToString();
            txtIva.Text = Iva.ToString();
            txtTotal.Text = total.ToString();
        }

        private void dgvProductoFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularFacturaTotal();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrProductoFactura = dgvProductoFactura.SelectedRows;
            if (dgrProductoFactura.Count == 0)
            {
                MessageBox.Show(this, "ERROR, no hay filas para eliminar!!", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataRow drProductofactura = ((DataRowView)dgrProductoFactura[0].DataBoundItem).Row;
            dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductofactura);

            CalcularFacturaTotal();

            // Busqueda en el combobox empleado
         

            }
     
        private void UpdateData()
        {
            if (cmbEmpleado.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleados"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombre = dataRow.Field<String>("Nombre"),
                        Apellido = dataRow.Field<String>("Apellido")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombre.Contains(cmbEmpleado.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleado.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleado.DataSource = dataSource;

                var sText = cmbEmpleado.Items[0].ToString();
                cmbEmpleado.SelectionStart = text.Length;
                cmbEmpleado.SelectionLength = sText.Length - text.Length;
                cmbEmpleado.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleado.DroppedDown = false;
                cmbEmpleado.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void cmbEmpleado_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void cmbEmpleado_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }

        }

        private void cmbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }

        private void cmbProductos_TextUpdate(object sender, EventArgs e)
        {
            MessageBox.Show(cmbProducto.SelectedValue.ToString());
        }


        private void cmbProductos_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            txtCantidad.Text = ((DataRowView)cmbProducto.SelectedItem).Row["Cantidad"].ToString();
            txtPrecio.Text = ((DataRowView)cmbProducto.SelectedItem).Row["Precio"].ToString();

        }

        private void btnfactura_Click(object sender, EventArgs e)
        {
            if(dsSistema.Tables["ProductoFactura"].Rows.Count == 0)
            {
                MessageBox.Show(this, "Error, no se puede generar la factura, revise si hay productos", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["CodFactura"] = cod_Factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtObservaciones.Text;
            drFactura["Empleado"] = cmbEmpleado.SelectedValue;
            drFactura["SubTotal"] = subtotal;
            drFactura["Iva"] = Iva;
            drFactura["Total"] = total;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);
            foreach (DataRow dr in dsSistema.Tables ["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);

                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);

                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString())-
                    Double.Parse(dr["Cantidad"].ToString());

            }

            FrmReporteFactura frf = new FrmReporteFactura();
            frf.MdiParent = this.MdiParent;
            frf.Dssitema = dsSistema;
            frf.Show();

            Dispose();
        }

        private void cmbProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProducto = ((DataRowView)cmbProducto.SelectedItem).Row;
            txtCantidad.Text = drProducto["Cantidad"].ToString();
            txtPrecio.Text = drProducto["Precio"].ToString();
        }



        //Update data when timer stops


    }
}


    
    

