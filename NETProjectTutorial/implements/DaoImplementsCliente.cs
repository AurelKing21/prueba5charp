﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.implements
{
    class DaoImplementsCliente:IDaoCliente
    {
        //header cliente
        private BinaryReader brhcliente;
        private BinaryWriter bwhcliente;
        //data cliente
        private BinaryReader brdcliente;
        private BinaryWriter bwdcliente;
        private FileStream fshccliente;
        private FileStream fsdccliente;

        private const string FILENAME_HEADER = "hcliente.dat";
        private const string FILENAME_DATA = "dcliente.dat";
        private const int SIZE = 390;

        public DaoImplementsCliente()
        {
        }
        private void open()
        {
            try
            {
                fsdccliente = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshccliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    //HEADER
                    brhcliente = new BinaryReader(fshccliente);
                    bwhcliente = new BinaryWriter(fshccliente);
                    //DATA
                    brdcliente = new BinaryReader(fsdccliente);
                    bwhcliente = new BinaryWriter(fsdccliente);

                    bwhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhcliente.Write(0);//n
                    bwhcliente.Write(0);//k
                }
            }
            catch(IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        private void close()
        {
            try
            {
                if (brdcliente != null)
                {
                    brdcliente.Close();
                }
                if (brhcliente != null)
                {
                    brhcliente.Close();
                }
                if (bwdcliente != null)
                {
                    bwdcliente.Close();
                }
                if (bwhcliente != null)
                {
                    bwhcliente.Close();
                }
                if (fsdccliente != null)
                {
                    fsdccliente.Close();

                }
                if (fshccliente != null)
                {
                    fshccliente.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public Cliente findById(int id)
        {
            throw new NotImplementedException();
        }

        public Cliente findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Cliente t)
        {
            open();
            brhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhcliente.ReadInt32();
            int k = brhcliente.ReadInt32();

            long dpos = k * SIZE;
            bwdcliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdcliente.Write(++k);
            bwdcliente.Write(t.Ced);
            bwdcliente.Write(t.Nombres);
            bwdcliente.Write(t.Apellidos);
            bwdcliente.Write(t.Telefono);
            bwdcliente.Write(t.Correo);
            bwdcliente.Write(t.Direccion);

            bwhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhcliente.Write(++n);
            bwhcliente.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhcliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhcliente.Write(k);
            close(); 
        }

        public int update(Cliente t)
        {

            throw new NotImplementedException();
        }

        public bool delete(Cliente t)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findAll()
        {
            open();
            List<Cliente> clientes = new List<Cliente>();

            brhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhcliente.ReadInt32();
            for(int i = 0; i< n; i++)
            {
                //calculamos posicion de la cabecera
                long hpos = 8 + i * 4;
                brhcliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhcliente.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index-1) * SIZE;
                brdcliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdcliente.ReadInt32();
                string ced = brdcliente.ReadString();
                string nombres= brdcliente.ReadString();
                string apellidos= brdcliente.ReadString();
                int tel = brdcliente.ReadInt32();
                string correo = brdcliente.ReadString();
                string dir = brdcliente.ReadString();
                Cliente c = new Cliente(id, ced, nombres, apellidos, tel, correo, dir);
                clientes.Add(c);
            }

            close();
            return clientes;
        }
    }
}
